const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');

gulp.task('default', ['slate-scss-watch', 'slate-js-watch']);

gulp.task('slate-scss-watch', function() {
  gulp.watch('dist/assets/*.scss.liquid', ['sass-autoprefixer']);
});

gulp.task('slate-js-watch', function() {
  gulp.watch(
    ['dist/assets/*.js', '!dist/assets/*.min.js'],
    ['js-minify']
  );
});

gulp.task('sass-autoprefixer', function() {
  return gulp
    .src('dist/assets/*.scss.liquid')
    .pipe(
      sass({
        // Values: nested, expanded, compact, compressed
        outputStyle: 'compressed'
      }).on('error', sass.logError)
    )
    .pipe(
      autoprefixer({
        cascade: false
      })
    )
    .pipe(
      rename(function(path) {
        path.basename = path.basename.replace(/\.scss/gi, '');
        path.extname = '.css.liquid';
      })
    )
    .pipe(gulp.dest('dist/assets'));
});

gulp.task('js-minify', function() {
  return gulp
    .src(['dist/assets/*.js', '!dist/assets/*.min.js'])
    .pipe(
      uglify()
    )
    .pipe(
      rename(function(path) {
        path.extname = '.min.js';
      })
    )
    .pipe(gulp.dest('dist/assets'));
});
